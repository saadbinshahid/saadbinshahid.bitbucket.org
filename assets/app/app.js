﻿(function () {

    console.log('Initializing... Please wait...')

    var app = angular.module("saadwebapp", []);


    var data = [{  
        
        specStack: [
            {
                name: 'Custom Software Development',
                icon: 'ion-settings'
            }, {
                name: 'Enterprise Data Solutions',
                icon: 'ion-android-storage'
            }, {
                name: 'Web Development',
                icon: 'ion-ios7-monitor'
            }
        ],

        techStack: [
        
            {
                name: 'html5',
                src: "assets/devicon-master/icons/html5/html5-original-wordmark.svg"
            }, {
                name: 'css3',
                src: "assets/devicon-master/icons/css3/css3-original-wordmark.svg"
            }, {
                name: 'bootstrap',
                src: "assets/devicon-master/icons/bootstrap/bootstrap-plain-wordmark.svg"
            }, {
                name: 'javascript',
                src: "assets/devicon-master/icons/javascript/javascript-original.svg"
            }, {
                name: 'jquery',
                src: "assets/devicon-master/icons/jquery/jquery-original-wordmark.svg"
            }, {
                name: 'linux',
                src: "assets/devicon-master/icons/linux/linux-original.svg"
            }, {
                name: 'mysql',
                src: "assets/devicon-master/icons/mysql/mysql-original-wordmark.svg"
            }, {
                name: 'php',
                src: "assets/devicon-master/icons/php/php-original.svg"
            }, {
                name: 'codeigniter',
                src: "assets/devicon-master/icons/codeigniter/codeigniter-plain-wordmark.svg"
            }, {
                name: 'ubuntu',
                src: "assets/devicon-master/icons/ubuntu/ubuntu-plain-wordmark.svg"
            }, {
                name: 'java',
                src: "assets/devicon-master/icons/java/java-original-wordmark.svg"
            }, {
                name: 'chrome',
                src: "assets/devicon-master/icons/chrome/chrome-original-wordmark.svg"
            }, {
                name: 'windows8',
                src: "assets/devicon-master/icons/windows8/windows8-original.svg"
            }, {
                name: 'dot-net',
                src: "assets/devicon-master/icons/dot-net/dot-net-original-wordmark.svg"
            }, {
                name: 'firefox',
                src: "assets/devicon-master/icons/firefox/firefox-original-wordmark.svg"
            }, {
                name: 'firefox',
                src: "assets/devicon-master/icons/angularjs/angularjs-original.svg"
            }, {
                name: 'git',
                src: "assets/devicon-master/icons/git/git-original.svg"
            }
        ],

        contactStack: [

            {
                name: 'github',
                href: 'https://github.com/saadbinshahid',
                icon: 'ion-social-github'
            },
            {
                name: 'skype',
                href: 'skype:saadbinshahid.fb?chat',
                icon: 'ion-social-skype'
            },
            {
                name: 'gmail',
                href: 'mailto:saadbinshahid.fb@gmail.com',
                icon: 'ion-email'
            },
            {
                name: 'linkedin',
                href: 'https://pk.linkedin.com/in/saadbinshahid',
                icon: 'ion-social-linkedin'
            },
            {
                name: 'twitter',
                href: 'https://twitter.com/SaadBinShahid',
                icon: 'ion-social-twitter'
            },
            {
                name: 'google-plus',
                href: 'https://plus.google.com/+SaadBinShahid',
                icon: 'ion-icon-social-google-plus'
            },
            {
                name: 'facebook',
                href: 'https://www.facebook.com/Saadbinshahid.fb',
                icon: 'ion-social-facebook'
            },
            {
                name: 'youtube',
                href: '',
                icon: 'ion-social-youtube'
            }
        ],

        credits: [
            {
                name: 'Black Tie - Basic theme',
                href: 'http://www.blacktie.co/2014/10/basic-personal-page/'
            }, {
                name: 'Ionicons',
                href: 'https://github.com/driftyco/ionicons'
            }, {
                name: 'Font Mfizz',
                href: 'http://mfizz.com/oss/font-mfizz'
            }, {
                name: 'AngularJS',
                href: 'https://angularjs.org/'
            }
        ]
        
    }];



    app.controller('dataController', function () {
        this.data = data;
    });



    console.log('we are good to Go .!')

})();